package pl.kasprowski.spring5webfluxrest.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.spring5webfluxrest.domain.Category;

@Repository
public interface CategoryRepository extends ReactiveMongoRepository<Category, String> {
}
