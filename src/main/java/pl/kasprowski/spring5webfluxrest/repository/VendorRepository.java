package pl.kasprowski.spring5webfluxrest.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.spring5webfluxrest.domain.Vendor;

@Repository
public interface VendorRepository extends ReactiveMongoRepository<Vendor, String> {
}
