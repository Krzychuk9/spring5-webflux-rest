package pl.kasprowski.spring5webfluxrest.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.kasprowski.spring5webfluxrest.domain.Category;
import pl.kasprowski.spring5webfluxrest.domain.Vendor;
import pl.kasprowski.spring5webfluxrest.repository.CategoryRepository;
import pl.kasprowski.spring5webfluxrest.repository.VendorRepository;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
public class Bootstrap implements CommandLineRunner {

    private final CategoryRepository categoryRepository;
    private final VendorRepository vendorRepository;

    @Autowired
    public Bootstrap(CategoryRepository categoryRepository, VendorRepository vendorRepository) {
        this.categoryRepository = categoryRepository;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (categoryRepository.count().block() == 0) {
            loadCategories();
        }
        if (vendorRepository.count().block() == 0) {
            loadVendors();
        }
    }

    private void loadCategories() {
        Set<Category> categories = new HashSet<>();

        categories.add(Category
                .builder()
                .description("Fruits")
                .build());

        categories.add(Category
                .builder()
                .description("Dried")
                .build());

        categories.add(Category
                .builder()
                .description("Fresh")
                .build());

        categories.add(Category
                .builder()
                .description("Nuts")
                .build());

        categories.add(Category
                .builder()
                .description("Exotic")
                .build());

        this.categoryRepository.saveAll(categories).blockFirst();
        log.info("Categories loaded!");
    }

    private void loadVendors() {
        Set<Vendor> vendors = new HashSet<>();

        vendors.add(Vendor
                .builder()
                .firstName("Western Tasty Fruits Ltd.")
                .lastName("vendor 1")
                .build());

        vendors.add(Vendor
                .builder()
                .firstName("Exotic Fruits Company")
                .lastName("vendor 2")
                .build());

        vendors.add(Vendor
                .builder()
                .firstName("Home Fruits")
                .lastName("vendor 3")
                .build());

        this.vendorRepository.saveAll(vendors).blockFirst();
        log.info("Vendors loaded!");
    }
}
