package pl.kasprowski.spring5webfluxrest.controllers;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.kasprowski.spring5webfluxrest.domain.Vendor;
import pl.kasprowski.spring5webfluxrest.repository.VendorRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/vendors")
public class VendorController {

    private final VendorRepository vendorRepository;

    @Autowired
    public VendorController(VendorRepository vendorRepository) {
        this.vendorRepository = vendorRepository;
    }

    @GetMapping
    public Flux<Vendor> findAll() {
        return this.vendorRepository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<Vendor> findById(@PathVariable String id) {
        return this.vendorRepository.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Void> create(@RequestBody Publisher<Vendor> vendorsStream) {
        return this.vendorRepository.saveAll(vendorsStream).then();
    }

    @PutMapping("/{id}")
    public Mono<Vendor> update(@PathVariable String id, @RequestBody Vendor vendor) {
        vendor.setId(id);
        return this.vendorRepository.save(vendor);
    }

    @PatchMapping("/{id}")
    public Mono<Vendor> patch(@PathVariable String id, @RequestBody Vendor vendor) {
        boolean isUpdated = false;

        Vendor foundVendor = this.vendorRepository.findById(id).block();

        String firstName = vendor.getFirstName();
        String lastName = vendor.getLastName();

        if (firstName != null && !firstName.equals(foundVendor.getFirstName())) {
            foundVendor.setFirstName(firstName);
            isUpdated = true;
        }

        if (lastName != null && !lastName.equals(foundVendor.getLastName())) {
            foundVendor.setLastName(lastName);
            isUpdated = true;
        }

        if (isUpdated) {
            return this.vendorRepository.save(foundVendor);
        }

        return Mono.just(foundVendor);
    }
}
