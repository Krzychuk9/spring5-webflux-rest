package pl.kasprowski.spring5webfluxrest.controllers;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.kasprowski.spring5webfluxrest.domain.Category;
import pl.kasprowski.spring5webfluxrest.repository.CategoryRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping
    public Flux<Category> findAll() {
        return this.categoryRepository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<Category> findById(@PathVariable String id) {
        return this.categoryRepository.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Mono<Void> create(@RequestBody Publisher<Category> categoryStream) {
        return this.categoryRepository.saveAll(categoryStream).then();
    }

    @PutMapping("/{id}")
    public Mono<Category> update(@PathVariable String id, @RequestBody Category category) {
        category.setId(id);
        return this.categoryRepository.save(category);
    }

    @PatchMapping("/{id}")
    public Mono<Category> patch(@PathVariable String id, @RequestBody Category category) {
        Category foundCategory = this.categoryRepository.findById(id).block();

        if (!category.getDescription().equals(foundCategory.getDescription())) {
            foundCategory.setDescription(category.getDescription());
            return this.categoryRepository.save(foundCategory);
        }

        return Mono.just(foundCategory);
    }
}
