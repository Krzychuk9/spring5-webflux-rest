package pl.kasprowski.spring5webfluxrest.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.reactivestreams.Publisher;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.kasprowski.spring5webfluxrest.domain.Category;
import pl.kasprowski.spring5webfluxrest.repository.CategoryRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class CategoryControllerTest {

    @Mock
    private CategoryRepository categoryRepository;
    @InjectMocks
    private CategoryController categoryController;
    private WebTestClient webTestClient;

    @Before
    public void setUp() throws Exception {
        webTestClient = WebTestClient.bindToController(categoryController).build();
    }

    @Test
    public void findAll() throws Exception {
        given(categoryRepository.findAll())
                .willReturn(Flux.just(Category.builder().description("cat1").build(), Category.builder().description("cat2").build()));

        webTestClient
                .get()
                .uri("api/v1/categories")
                .exchange()
                .expectBodyList(Category.class)
                .hasSize(2);
    }

    @Test
    public void findById() throws Exception {
        given(categoryRepository.findById(anyString()))
                .willReturn(Mono.just(Category.builder().description("cat1").build()));

        webTestClient
                .get()
                .uri("api/v1/categories/123")
                .exchange()
                .expectBody(Category.class);

        verify(categoryRepository).findById(eq("123"));
    }

    @Test
    public void create() throws Exception {
        given(categoryRepository.saveAll(any(Publisher.class)))
                .willReturn(Flux.just(Category.builder().description("desc").build()));

        Mono<Category> categoryToBeSaved = Mono.just(Category.builder().description("same cat").build());

        webTestClient
                .post()
                .uri("api/v1/categories")
                .body(categoryToBeSaved, Category.class)
                .exchange()
                .expectStatus()
                .isCreated();
    }

    @Test
    public void update() throws Exception {
        given(categoryRepository.save(any(Category.class)))
                .willReturn(Mono.just(Category.builder().description("desc").build()));

        Mono<Category> categoryToBeUpdated = Mono.just(Category.builder().id("123").description("desc").build());

        webTestClient
                .put()
                .uri("api/v1/categories/123")
                .body(categoryToBeUpdated, Category.class)
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    public void patchWithChanges() throws Exception {
        ArgumentCaptor<Category> captor = ArgumentCaptor.forClass(Category.class);
        Mono<Category> savedCategoryMono = Mono.just(Category.builder().description("description").build());

        given(categoryRepository.findById(anyString()))
                .willReturn(savedCategoryMono);

        given(categoryRepository.save(any(Category.class)))
                .willReturn(savedCategoryMono);

        Mono<Category> categoryToBeUpdated = Mono.just(Category.builder().description("new description").build());

        webTestClient
                .patch()
                .uri("api/v1/categories/123")
                .body(categoryToBeUpdated, Category.class)
                .exchange()
                .expectStatus()
                .isOk();

        verify(categoryRepository).findById(eq("123"));
        verify(categoryRepository).save(captor.capture());
        assertEquals("new description", captor.getValue().getDescription());
    }

    @Test
    public void patchWithoutChanges() throws Exception {
        ArgumentCaptor<Category> captor = ArgumentCaptor.forClass(Category.class);
        Mono<Category> savedCategoryMono = Mono.just(Category.builder().description("description").build());

        given(categoryRepository.findById(anyString()))
                .willReturn(savedCategoryMono);

        given(categoryRepository.save(any(Category.class)))
                .willReturn(savedCategoryMono);

        Mono<Category> categoryToBeUpdated = Mono.just(Category.builder().description("description").build());

        webTestClient
                .patch()
                .uri("api/v1/categories/123")
                .body(categoryToBeUpdated, Category.class)
                .exchange()
                .expectStatus()
                .isOk();

        verify(categoryRepository).findById(eq("123"));
        verifyNoMoreInteractions(categoryRepository);
    }
}