package pl.kasprowski.spring5webfluxrest.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.reactivestreams.Publisher;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.kasprowski.spring5webfluxrest.domain.Vendor;
import pl.kasprowski.spring5webfluxrest.repository.VendorRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class VendorControllerTest {

    @Mock
    private VendorRepository vendorRepository;
    @InjectMocks
    private VendorController vendorController;
    private WebTestClient webTestClient;

    @Before
    public void setUp() throws Exception {
        webTestClient = WebTestClient.bindToController(vendorController).build();
    }

    @Test
    public void findAll() throws Exception {
        given(vendorRepository.findAll())
                .willReturn(Flux.just(
                        Vendor.builder().firstName("name1").lastName("last1").build(),
                        Vendor.builder().firstName("name2").lastName("last2").build()));

        webTestClient
                .get()
                .uri("/api/v1/vendors")
                .exchange()
                .expectBodyList(Vendor.class)
                .hasSize(2);

        verify(vendorRepository).findAll();
    }

    @Test
    public void findById() throws Exception {
        given(vendorRepository.findById(anyString()))
                .willReturn(Mono.just(Vendor.builder().firstName("name").lastName("last").build()));

        webTestClient
                .get()
                .uri("/api/v1/vendors/123")
                .exchange()
                .expectBody(Vendor.class);

        verify(vendorRepository).findById(eq("123"));
    }

    @Test
    public void create() throws Exception {
        given(vendorRepository.saveAll(any(Publisher.class)))
                .willReturn(Flux.just(Vendor.builder().firstName("first").lastName("last").build()));

        Mono<Vendor> vendorToBeCreated = Mono.just(Vendor.builder().firstName("first").lastName("last").build());

        webTestClient
                .post()
                .uri("/api/v1/vendors")
                .body(vendorToBeCreated, Vendor.class)
                .exchange()
                .expectStatus()
                .isCreated();
    }

    @Test
    public void update() throws Exception {
        given(vendorRepository.save(any(Vendor.class)))
                .willReturn(Mono.just(Vendor.builder().firstName("first").lastName("last").id("123").build()));

        Mono<Vendor> vendorToBeUpdated = Mono.just(Vendor.builder().firstName("first").lastName("last").id("123").build());

        webTestClient
                .put()
                .uri("/api/v1/vendors/123")
                .body(vendorToBeUpdated, Vendor.class)
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    public void patchWithChanges() throws Exception {
        ArgumentCaptor<Vendor> captor = ArgumentCaptor.forClass(Vendor.class);

        Mono<Vendor> savedVendorMono = Mono.just(Vendor.builder().firstName("first").lastName("last").build());
        Mono<Vendor> vendorToBeUpdated = Mono.just(Vendor.builder().firstName("new first name").build());

        given(vendorRepository.findById(anyString()))
                .willReturn(savedVendorMono);
        given(vendorRepository.save(any(Vendor.class)))
                .willReturn(savedVendorMono);

        webTestClient
                .patch()
                .uri("/api/v1/vendors/123")
                .body(vendorToBeUpdated, Vendor.class)
                .exchange()
                .expectStatus()
                .isOk();

        verify(vendorRepository).findById(eq("123"));
        verify(vendorRepository).save(captor.capture());
        assertEquals("new first name", captor.getValue().getFirstName());
    }

    @Test
    public void patchWithoutChanges() throws Exception {
        Mono<Vendor> savedVendorMono = Mono.just(Vendor.builder().firstName("first").lastName("last").build());
        Mono<Vendor> vendorToBeUpdated = Mono.just(Vendor.builder().firstName("first").build());

        given(vendorRepository.findById(anyString()))
                .willReturn(savedVendorMono);

        webTestClient
                .patch()
                .uri("/api/v1/vendors/123")
                .body(vendorToBeUpdated, Vendor.class)
                .exchange()
                .expectStatus()
                .isOk();

        verify(vendorRepository).findById(eq("123"));
        verifyNoMoreInteractions(vendorRepository);
    }
}